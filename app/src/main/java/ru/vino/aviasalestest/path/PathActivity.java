package ru.vino.aviasalestest.path;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;
import ru.vino.aviasalestest.R;
import ru.vino.aviasalestest.model.IataModel;

public class PathActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String EXTRA_START = "ru.vino.aviasalestest.path.EXTRA_START";
    public static final String EXTRA_END = "ru.vino.aviasalestest.path.EXTRA_END";

    private static final int DOT_DENSITY = 30;
    private static final float ANIMATION_TIME = 15000;

    IataModel start;
    IataModel end;

    @BindView(R.id.root)
    ViewGroup root;

    @State
    float planePathPercent;
    @State
    long startTime;

    PathMeasure pathMeasure;
    Marker plane;

    GoogleMap map;
    Projection projection;

    List<Marker> dots = new ArrayList<>();
    Bitmap circleBitmap;

    float lastZoom;

    Interpolator interpolator = new LinearInterpolator();

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        Icepick.restoreInstanceState(this, savedInstanceState);

        handler = new Handler();

        start = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_START));
        end = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_END));

        int px = getResources().getDimensionPixelSize(R.dimen.map_dot_size);
        circleBitmap = Bitmap.createBitmap(px, px, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(circleBitmap);
        Drawable shape = getResources().getDrawable(R.drawable.marker_dot);
        shape.setBounds(0, 0, circleBitmap.getWidth(), circleBitmap.getHeight());
        shape.draw(canvas);

        final MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);

        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= 16) {
                    root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                root.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                mapFragment.getMapAsync(PathActivity.this);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        View city = getLayoutInflater().inflate(R.layout.marker_airport, root, false);
        TextView iata = (TextView) city.findViewById(R.id.iata);
        iata.setText(start.getIata());
        drawCity(start.getLatLng(), city);
        iata.setText(end.getIata());
        drawCity(end.getLatLng(), city);

        googleMap.setOnCameraIdleListener(() -> {
            if (map.getCameraPosition().zoom != lastZoom) {
                lastZoom = map.getCameraPosition().zoom;
                projection = map.getProjection();
                Point startPoint = projection.toScreenLocation(start.getLatLng());
                Point endPoint = projection.toScreenLocation(end.getLatLng());
                drawPath(startPoint, endPoint);
            }
        });
    }

    private void drawCircle(LatLng latLng) {
        MarkerOptions markerOptions = new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(circleBitmap))
                .anchor(0.5f, 0.5f)
                .position(latLng);

        dots.add(map.addMarker(markerOptions));
    }

    private void drawCity(LatLng latLng, View view) {
        map.addMarker(new MarkerOptions()
                .alpha(0.8f)
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(view)))
                .position(latLng));
    }

    private void drawPath(Point point1, Point point2) {
        for (Marker dot : dots)
            dot.remove();
        dots.clear();

        float angle = (float) getAngle(point2, point1);

        Point midPoint = new Point((point1.x + point2.x) / 2, (point1.y + point2.y) / 2);

        float dist = (float) Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2));
        Path path = new Path();
        path.moveTo(point1.x, point1.y);

        float bezierDelta = dist / 1.5f;
        if (angle < 45) {
            if (point1.y > point2.y)
                bezierDelta = -bezierDelta;
            path.cubicTo((point1.x + midPoint.x) / 2, point1.y + bezierDelta,
                    (point2.x + midPoint.x) / 2, point2.y - bezierDelta, point2.x, point2.y);
        } else {
            if (point1.x > point2.x)
                bezierDelta = -bezierDelta;
            path.cubicTo(point1.x + bezierDelta, (point1.y + midPoint.y) / 2,
                    point2.x - bezierDelta, (point2.y + midPoint.y) / 2, point2.x, point2.y);
        }

        pathMeasure = new PathMeasure(path, false);
        int dotsCount = (int) (pathMeasure.getLength() / (getResources().getDimensionPixelSize(R.dimen.map_dot_size) + pathMeasure.getLength() / DOT_DENSITY));
        float[] dotPos = new float[2];

        for (int i = 1; i < dotsCount; i++) {
            pathMeasure.getPosTan((pathMeasure.getLength() * (float) i / (float) dotsCount), dotPos, null);
            Point dotPoint = new Point((int) dotPos[0], (int) dotPos[1]);
            LatLng dotLatLng = projection.fromScreenLocation(dotPoint);
            drawCircle(dotLatLng);
        }

        animatePlane();
    }

    public double getAngle(Point p1, Point p2) {
        double angle;
        if (p1.x >= p2.x)
            angle = Math.toDegrees(Math.atan2(p2.y - p1.y, p2.x - p1.x));
        else angle = Math.toDegrees(Math.atan2(p1.y - p2.y, p1.x - p2.x));

        if (angle < 0) {
            angle += 360;
        }

        return Math.abs(180 - angle);
    }

    private void animatePlane() {
        if (plane == null) {
            final float[] pos = new float[2];
            final float[] tan = new float[2];
            pathMeasure.getPosTan(pathMeasure.getLength() * planePathPercent, pos, tan);

            plane = map.addMarker(new MarkerOptions()
                    .alpha(0.8f)
                    .anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_plane))
                    .rotation((float) Math.toDegrees(Math.atan2(tan[1], tan[0])))
                    .position(projection.fromScreenLocation(new Point((int) pos[0], (int) pos[1]))));

            if (startTime == 0)
                startTime = SystemClock.uptimeMillis();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    long elapsed = SystemClock.uptimeMillis() - startTime;
                    planePathPercent = interpolator.getInterpolation(elapsed / ANIMATION_TIME);
                    pathMeasure.getPosTan(pathMeasure.getLength() * planePathPercent, pos, tan);
                    plane.setPosition(projection.fromScreenLocation(new Point((int) pos[0], (int) pos[1])));
                    plane.setRotation((float) Math.toDegrees(Math.atan2(tan[1], tan[0])));
                    if (planePathPercent < 1)
                        handler.postDelayed(this, 16);
                }
            });
        }
    }

    public Bitmap loadBitmapFromView(View v) {
        v.measure(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(start.getLatLng());
        builder.include(end.getLatLng());
        LatLngBounds bounds = builder.build();
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, v.getMeasuredWidth() / 2));

        return b;
    }

}
