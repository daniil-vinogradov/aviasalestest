package ru.vino.aviasalestest.di.modules;


import dagger.Module;
import dagger.Provides;
import ru.vino.aviasalestest.di.ActivityScope;
import ru.vino.aviasalestest.interactors.IAviasalesInteractor;
import ru.vino.aviasalestest.search.SearchContract;
import ru.vino.aviasalestest.search.SearchPresenterImpl;

@Module
public class SearchActivityModule {

    @ActivityScope
    @Provides
    SearchContract.AbstractSearchPresenter provideSearchPresenter(IAviasalesInteractor interactor) {
        return new SearchPresenterImpl(interactor);
    }

}
