package ru.vino.aviasalestest.di.components;

import dagger.Subcomponent;
import ru.vino.aviasalestest.di.ActivityScope;
import ru.vino.aviasalestest.di.modules.SearchActivityModule;
import ru.vino.aviasalestest.search.SearchActivity;

@ActivityScope
@Subcomponent(modules = {SearchActivityModule.class})
public interface SearchActivityComponent {

    void inject(SearchActivity searchActivity);

}
