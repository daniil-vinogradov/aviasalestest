package ru.vino.aviasalestest.di.components;


import javax.inject.Singleton;

import dagger.Component;
import ru.vino.aviasalestest.di.modules.ApiModule;
import ru.vino.aviasalestest.di.modules.AppModule;

@Singleton
@Component(modules = {
        ApiModule.class,
        AppModule.class
})
public interface AppComponent {

    SearchActivityComponent createSearchActivityComponent();

}
