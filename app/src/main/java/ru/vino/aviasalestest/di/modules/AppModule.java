package ru.vino.aviasalestest.di.modules;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import ru.vino.aviasalestest.AviasalesTestApplication;
import ru.vino.aviasalestest.interactors.AviasalesInteractorImpl;
import ru.vino.aviasalestest.interactors.IAviasalesInteractor;

@Module
public class AppModule {

    AviasalesTestApplication application;

    public AppModule(AviasalesTestApplication application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Context provideApplicationContext() {
        return application;
    }

    @Singleton
    @Provides
    IAviasalesInteractor provideInteractor(Retrofit retrofit) {
        return new AviasalesInteractorImpl(retrofit);
    }

}
