package ru.vino.aviasalestest.search;


import ru.vino.aviasalestest.base.BasePresenter;
import ru.vino.aviasalestest.model.IataModel;

public interface SearchContract {

    interface ISearchView {
        void showProgress();

        void showConnectionError();

        void showDepartureNotFound();

        void showArriveNotFound();

        void showPath(IataModel start, IataModel end);
    }

    abstract class AbstractSearchPresenter extends BasePresenter<ISearchView> {
        public abstract void search(String start, String end);
    }

}
