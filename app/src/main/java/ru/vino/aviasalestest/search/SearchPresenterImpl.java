package ru.vino.aviasalestest.search;


import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.NoSuchElementException;

import ru.vino.aviasalestest.interactors.IAviasalesInteractor;
import ru.vino.aviasalestest.model.IataModel;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SearchPresenterImpl extends SearchContract.AbstractSearchPresenter {

    private IAviasalesInteractor interactor;
    private boolean isInLoading;

    public SearchPresenterImpl(IAviasalesInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void attachView(SearchContract.ISearchView view) {
        super.attachView(view);
        if (isInLoading)
            view.showProgress();
    }

    @Override
    public void search(String start, String end) {

        if (isInLoading)
            return;
        isInLoading = true;

        if (isViewAttached())
            getView().showProgress();

        Observable.concat(getSearchObservable(start),
                getSearchObservable(end))
                .toList()
                .doOnNext(iataModels -> {
                    if (iataModels.size() != 2)
                        throw new RuntimeException("Unknown error");
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(iataModels -> {
                    if (isViewAttached()) {
                        if (iataModels.get(0) == null)
                            getView().showDepartureNotFound();
                        if (iataModels.get(1) == null)
                            getView().showArriveNotFound();
                    }
                    if (iataModels.get(0) == null || iataModels.get(1) == null)
                        throw new RuntimeException("City not found");
                })
                .subscribe(iataModels -> {
                    isInLoading = false;
                    if (isViewAttached())
                        getView().showPath(iataModels.get(0), iataModels.get(1));
                }, throwable -> {
                    throwable.printStackTrace();
                    isInLoading = false;
                    if (isViewAttached()) {
                        if (throwable.getCause() instanceof IOException)
                            getView().showConnectionError();
                    }
                });

    }

    /**
     * Just take first iata from response
     */
    private Observable<IataModel> getSearchObservable(String term) {
        return interactor
                .search(term)
                .flatMap(searchResponseModel -> Observable.from(searchResponseModel.getCities()))
                .filter(cityModel -> cityModel.getIata() != null && !cityModel.getIata().isEmpty())
                .first()
                .map(cityModel -> {
                    String iata = cityModel.getIata().get(0);
                    LatLng latLng = cityModel.getLocation().getLatLng();
                    return new IataModel(iata, latLng);
                })
                .onErrorReturn(throwable -> {
                    if (throwable instanceof NoSuchElementException)
                        return null;
                    else throw new RuntimeException(throwable);
                });
    }

}
