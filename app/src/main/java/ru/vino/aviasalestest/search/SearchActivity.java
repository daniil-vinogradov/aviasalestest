package ru.vino.aviasalestest.search;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.vino.aviasalestest.AviasalesTestApplication;
import ru.vino.aviasalestest.IntentStarter;
import ru.vino.aviasalestest.R;
import ru.vino.aviasalestest.base.ComponentActivity;
import ru.vino.aviasalestest.di.components.SearchActivityComponent;
import ru.vino.aviasalestest.model.IataModel;

public class SearchActivity extends ComponentActivity<SearchActivityComponent>
        implements SearchContract.ISearchView {

    @Inject
    SearchContract.AbstractSearchPresenter presenter;

    @BindView(R.id.root)
    ViewGroup root;
    @BindView(R.id.departure)
    TextView departure;
    @BindView(R.id.arrive)
    TextView arrive;
    @BindView(R.id.search)
    Button search;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @OnClick(R.id.search)
    void onSearchClick() {
        String departureText = departure.getText().toString();
        String arriveText = arrive.getText().toString();
        if (!departureText.trim().isEmpty() &&
                !arriveText.trim().isEmpty())
            presenter.search(departureText, arriveText);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        presenter.attachView(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public SearchActivityComponent setComponent() {
        return AviasalesTestApplication.from(this).getComponent().createSearchActivityComponent();
    }

    @Override
    public void onInject(SearchActivityComponent component) {
        component.inject(this);
    }

    @Override
    public void showProgress() {
        departure.setError(null);
        arrive.setError(null);
        hideSoftKeyboard();
        enableViews(false);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showConnectionError() {
        enableViews(true);
        progressBar.setVisibility(View.GONE);
        Snackbar.make(root, R.string.error_connection, Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showDepartureNotFound() {
        progressBar.setVisibility(View.GONE);
        enableViews(true);
        departure.setError(getString(R.string.error_not_found));
    }

    @Override
    public void showArriveNotFound() {
        progressBar.setVisibility(View.GONE);
        enableViews(true);
        arrive.setError(getString(R.string.error_not_found));
    }


    @Override
    public void showPath(IataModel start, IataModel end) {
        if (!start.getIata().equals(end.getIata()))
            IntentStarter.openPathActivity(this, start, end);
        enableViews(true);
        progressBar.setVisibility(View.GONE);
    }

    private void enableViews(boolean enable) {
        search.setEnabled(enable);
        departure.setEnabled(enable);
        arrive.setEnabled(enable);
    }

    private void hideSoftKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
