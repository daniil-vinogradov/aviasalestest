package ru.vino.aviasalestest;


import android.content.Context;
import android.content.Intent;

import org.parceler.Parcels;

import ru.vino.aviasalestest.model.IataModel;
import ru.vino.aviasalestest.path.PathActivity;

public class IntentStarter {

    private IntentStarter() {
    }

    public static void openPathActivity(Context context, IataModel start, IataModel end) {
        Intent intent = new Intent(context, PathActivity.class);
        intent.putExtra(PathActivity.EXTRA_START, Parcels.wrap(start));
        intent.putExtra(PathActivity.EXTRA_END, Parcels.wrap(end));
        context.startActivity(intent);
    }

}
