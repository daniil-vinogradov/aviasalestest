package ru.vino.aviasalestest.api;


import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.vino.aviasalestest.model.responsemodels.SearchResponseModel;
import rx.Observable;

public interface HotellookApi {

    String ENDPOINT_URL = "https://yasen.hotellook.com/";

    @GET("/autocomplete")
    Observable<SearchResponseModel> search(@Query("term") String term,
                                           @Query("lang") String lang);

}
