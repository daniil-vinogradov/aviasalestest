package ru.vino.aviasalestest.interactors;


import ru.vino.aviasalestest.model.responsemodels.SearchResponseModel;
import rx.Observable;

public interface IAviasalesInteractor {

    Observable<SearchResponseModel> search(String term);

}
