package ru.vino.aviasalestest.interactors;

import retrofit2.Retrofit;
import ru.vino.aviasalestest.api.HotellookApi;
import ru.vino.aviasalestest.model.responsemodels.SearchResponseModel;
import rx.Observable;


public class AviasalesInteractorImpl implements IAviasalesInteractor {

    Retrofit retrofit;

    public AviasalesInteractorImpl(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public Observable<SearchResponseModel> search(String term) {
        return retrofit.create(HotellookApi.class)
                .search(term, "ru");
    }

}
