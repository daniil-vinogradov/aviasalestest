package ru.vino.aviasalestest;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;

import com.squareup.leakcanary.LeakCanary;

import ru.vino.aviasalestest.di.components.AppComponent;
import ru.vino.aviasalestest.di.components.DaggerAppComponent;
import ru.vino.aviasalestest.di.modules.AppModule;


public class AviasalesTestApplication extends MultiDexApplication {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
    }

    public AppComponent getComponent() {
        return appComponent;
    }

    public static AviasalesTestApplication from(@NonNull Context context) {
        return (AviasalesTestApplication) context.getApplicationContext();
    }

}
