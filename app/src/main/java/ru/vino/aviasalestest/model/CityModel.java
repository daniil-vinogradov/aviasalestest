package ru.vino.aviasalestest.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityModel {

    @SerializedName("iata")
    @Expose
    List<String> iata = null;
    @SerializedName("location")
    @Expose
    LocationModel location;

    public List<String> getIata() {
        return iata;
    }

    public LocationModel getLocation() {
        return location;
    }

}
