package ru.vino.aviasalestest.model;


import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationModel {

    @SerializedName("lat")
    @Expose
    double lat;
    @SerializedName("lon")
    @Expose
    double lon;

    public LatLng getLatLng() {
        return new LatLng(lat, lon);
    }

}


