package ru.vino.aviasalestest.model;

import com.google.android.gms.maps.model.LatLng;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

@Parcel
public class IataModel {

    String iata;
    LatLng latLng;

    @ParcelConstructor
    public IataModel(String iata, LatLng latLng) {
        this.iata = iata;
        this.latLng = latLng;
    }

    public String getIata() {
        return iata;
    }

    public LatLng getLatLng() {
        return latLng;
    }

}
