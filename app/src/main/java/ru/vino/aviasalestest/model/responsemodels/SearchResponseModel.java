package ru.vino.aviasalestest.model.responsemodels;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.vino.aviasalestest.model.CityModel;

public class SearchResponseModel {

    @SerializedName("cities")
    @Expose
    List<CityModel> cities = null;

    public List<CityModel> getCities() {
        return cities;
    }

}
